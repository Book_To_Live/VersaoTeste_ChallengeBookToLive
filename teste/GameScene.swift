//
//  GameScene.swift
//  teste
//
//  Created by Renata Silva on 12/05/16.
//  Copyright (c) 2016 Renata Silva. All rights reserved.
//

import SpriteKit

enum Direction  {
    case LEFT
    case RIGHT
}
//teste victor
class GameScene: SKScene , SKPhysicsContactDelegate {
    
    //COLISAO

    struct PhysicsCategory {
        
        static let None: UInt32 = 1
        static let Square: UInt32 = 0b1
        static let Ball: UInt32 = 0b10
        static let Border: UInt32 = 0b100
        static let All: UInt32 = UInt32.max
        
    }
    
    func didBeginContact(contact: SKPhysicsContact) {
        
        var contactBody1: SKPhysicsBody
        var contactBody2: SKPhysicsBody
        
        if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask{
            contactBody1 = contact.bodyA
            contactBody2 = contact.bodyB
        }else{
            contactBody1 = contact.bodyB
            contactBody2 = contact.bodyA
        }
        
        if((contactBody1.categoryBitMask == 1) && (contactBody2.categoryBitMask == 2)){
//            //transicao para cena de conversa com baloes (PELA COLISAO)
//            let entrarScene2 = SKTransition.doorwayWithDuration(1.5)
//            let chat = ConversaScene(fileNamed: "ConversaScene")
//            self.view?.presentScene(chat!, transition: entrarScene2)
//            //fim
            contactBody2.node?.removeFromParent()
            
            contMaracuja = contMaracuja + 1

        }
        
    }
    
    
    //FIM
    
    var curupiraLeft: SKSpriteNode?
    var curupiraRight: SKSpriteNode?
    var porco: SKSpriteNode?
    var arrowLeft: SKSpriteNode?
    var arrowRight: SKSpriteNode?
    var pular: SKSpriteNode?
    var chave: SKSpriteNode?
    var cacador: SKSpriteNode?
    var balaoCurupira: SKSpriteNode?
    var balaoCacador: SKSpriteNode?
    var tap: SKSpriteNode?
    var quadroChave: SKSpriteNode?
    var maracuja: SKSpriteNode?
    
    var aux: SKSpriteNode?
    var speedT: CGFloat = 5
    var direction: Direction = Direction.RIGHT
    var buttonsCreated = [SKSpriteNode]()
    var pressedButtons = [SKSpriteNode]()
    var isJumping = false
    var verificar = true
    var maracujaRecolhido: SKLabelNode?
    var contMaracuja = 0
    
    override func didMoveToView(view: SKView) {
        
        //COLISAO
        self.physicsWorld.gravity = CGVectorMake(0, -9.8)
        physicsWorld.contactDelegate = self
        
        let sceneBody = SKPhysicsBody(edgeLoopFromRect: self.frame)
        sceneBody.friction = 0
        self.physicsBody = sceneBody
        //FIM
        
        curupiraRight = self.childNodeWithName("textureCurupira") as? SKSpriteNode
        curupiraLeft = self.childNodeWithName("textureCurupira") as? SKSpriteNode
        chave = self.childNodeWithName("textureChave") as? SKSpriteNode
        balaoCurupira = self.childNodeWithName("sprite_balaoCacador") as? SKSpriteNode
        balaoCacador = self.childNodeWithName("sprite_balaoCurupira") as? SKSpriteNode
        tap = self.childNodeWithName("spriteTap") as? SKSpriteNode
        cacador = self.childNodeWithName("textureCacador") as? SKSpriteNode
        quadroChave = self.childNodeWithName("texture_quadroChave") as? SKSpriteNode
        maracujaRecolhido = (self.childNodeWithName("labelMaracuja") as? SKLabelNode?)!
        
        
        arrowLeft = self.childNodeWithName("textureLeft") as? SKSpriteNode
        buttonsCreated.append(arrowLeft!)
        arrowRight = self.childNodeWithName("textureRight") as? SKSpriteNode
        buttonsCreated.append(arrowRight!)
        pular = self.childNodeWithName("texturePular") as? SKSpriteNode
        buttonsCreated.append(pular!)
        
        //COLISAO
        self.physicsBody?.categoryBitMask = PhysicsCategory.Border
        self.physicsBody?.collisionBitMask = PhysicsCategory.Ball
        self.physicsBody?.contactTestBitMask = PhysicsCategory.None
        
        curupiraRight!.physicsBody?.categoryBitMask = PhysicsCategory.Square
        curupiraRight!.physicsBody?.collisionBitMask = PhysicsCategory.None
        curupiraRight!.physicsBody?.contactTestBitMask = PhysicsCategory.Ball
        
        curupiraLeft!.physicsBody?.categoryBitMask = PhysicsCategory.Square
        curupiraLeft!.physicsBody?.collisionBitMask = PhysicsCategory.None
        curupiraLeft!.physicsBody?.contactTestBitMask = PhysicsCategory.Ball
        
        for i in 1...3{
            maracuja = self.childNodeWithName("spriteMaracuja\(i)") as? SKSpriteNode
            
            maracuja!.physicsBody?.categoryBitMask = PhysicsCategory.Ball
            maracuja!.physicsBody?.collisionBitMask = PhysicsCategory.Border | PhysicsCategory.Ball
            maracuja!.physicsBody?.contactTestBitMask = PhysicsCategory.Square
            
        }
        
        //FIM
        
    }
    
    override func update(currentTime: CFTimeInterval) {
        
        
        if (verificar) {
        if pressedButtons.indexOf(pular!) != nil {
            
            if curupiraLeft?.physicsBody?.velocity.dy == 0{
                    if pressedButtons.indexOf(arrowLeft!) != nil {
                        jump(Direction.LEFT)
                    } else if pressedButtons.indexOf(arrowRight!) != nil {
                        jump(Direction.RIGHT)
                    } else {
                        simpleJump()
                    }
                }
            }
            
            }
        
            if pressedButtons.indexOf(arrowLeft!) != nil {
                walk(Direction.LEFT)
            }
            if pressedButtons.indexOf(arrowRight!) != nil {
                walk(Direction.RIGHT)
            }
        
        maracujaRecolhido?.text = "\(contMaracuja)"
        
    }
    
    func walk(direction: Direction) {
        print(#function)
        if direction == Direction.RIGHT {
            curupiraLeft!.position = CGPoint(x: (curupiraLeft!.position.x) + speedT, y: curupiraLeft!.position.y)
        } else {
            curupiraLeft!.position = CGPoint(x: (curupiraLeft!.position.x) - speedT, y: curupiraLeft!.position.y)
        }
    }
    
    func simpleJump() {
        curupiraLeft!.physicsBody?.applyImpulse(CGVectorMake(0, 50))
    }
    
    func jump(direction: Direction) {
        if (direction == Direction.LEFT) {
            curupiraLeft!.physicsBody?.applyImpulse(CGVectorMake(-5, 50))
        } else {
            curupiraLeft!.physicsBody?.applyImpulse(CGVectorMake(5, 50))
        }
    }
    
    var cont = 0
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch: AnyObject in touches {
            let location = touch.locationInNode(self)
            for button in buttonsCreated {
                if button.containsPoint(location) && pressedButtons.indexOf(button) == nil {
                    pressedButtons.append(button)
                }
            }
        }
        
        
        for touch in touches {
            let location = (touch as UITouch).locationInNode (self)
            let toque = self.nodeAtPoint(location) as? SKSpriteNode
            
            if (toque != nil){
                
                //chave vai para o canto da cena
                if cont == 0 {
                    if toque!.name ==  "textureChave" {
                        chave?.position = CGPoint(x: quadroChave!.position.x, y: quadroChave!.position.y - 50)
                        cont = 1
                    }
                }
                //fim
            }
        }
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch: AnyObject in touches {
            let location = touch.locationInNode(self)
            let previousLocation = touch.previousLocationInNode(self)
            
            for button in buttonsCreated {
                if button.containsPoint(previousLocation) && !button.containsPoint(location) {
                    let index = pressedButtons.indexOf(button)
                    if index != nil {
                        pressedButtons.removeAtIndex(index!)
                    }
                } else if !button.containsPoint(previousLocation) && button.containsPoint(location) && pressedButtons.indexOf(button) == nil {
                    pressedButtons.append(button)
                }
            }
        }
        
        //arrastar a chave para libertar o porco
        if cont == 1{
            for toque in touches{
                let posicao = toque.locationInNode(self)
                chave?.position = CGPoint(x: posicao.x, y: posicao.y)
            }
        }
        //fim

    }
    
    func touchsEndOrCancelled(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch: AnyObject in touches {
            let location = touch.locationInNode(self)
            let previousLocation = touch.previousLocationInNode(self)
            
            for button in buttonsCreated {
                if button.containsPoint(location) {
                    let index = pressedButtons.indexOf(button)
                    if index != nil {
                        pressedButtons.removeAtIndex(index!)
                    }
                }
                else if (button.containsPoint(previousLocation)) {
                    let index = pressedButtons.indexOf(button)
                    if index != nil {
                        pressedButtons.removeAtIndex(index!)
                    }
                }
            }
        }
        
    }
    
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        touchsEndOrCancelled(touches, withEvent: event)
    }
    
    override func touchesCancelled(touches: Set<UITouch>?, withEvent event: UIEvent?) {
        touchsEndOrCancelled(touches!, withEvent: event)
    }

}
