//
//  ConversaScene.swift
//  teste
//
//  Created by Renata Silva on 13/05/16.
//  Copyright © 2016 Renata Silva. All rights reserved.
//

import UIKit
import SpriteKit

class ConversaScene: SKScene {
    
    var aux: SKSpriteNode?
    var curupiraRight: SKSpriteNode?
    var cacador: SKSpriteNode?
    
    var cont = 1
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        if cont > 4{
            
            //transicao de cena
            let entrarScene2 = SKTransition.doorwayWithDuration(1.0)
            let end = endHistory(fileNamed: "endHistory")
            self.view?.presentScene(end!, transition: entrarScene2)
            //fim
            
        }else{
            
            let balao = SKSpriteNode(imageNamed: "balao\(cont)")
            balao.xScale = 0.3
            balao.yScale = 0.3
            balao.zPosition = 20
            
            
            if (cont == 1) || (cont == 3){
                //cria o balao para o curupira
                aux?.removeFromParent()
                balao.position = CGPointMake(300, 430)
                self.addChild(balao)
                aux = balao
                
            }else{
                //cria o balao para o cacador
                aux?.removeFromParent()
                balao.position = CGPointMake(600, 450)
                self.addChild(balao)
                aux = balao
            }
            
        }
        
        cont = cont + 1
        
    }

}
