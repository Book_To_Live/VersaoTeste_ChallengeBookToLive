//
//  endHistory.swift
//  teste
//
//  Created by Renata Silva on 15/05/16.
//  Copyright © 2016 Renata Silva. All rights reserved.
//

import UIKit
import SpriteKit

class endHistory: SKScene {
    
    var backGame: SKSpriteNode?
    
    override func didMoveToView(view: SKView) {
        
        backGame = self.childNodeWithName("texture_backGame") as? SKSpriteNode
        
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch in touches {
            
            let location = (touch as UITouch).locationInNode (self)
            let toque = self.nodeAtPoint(location) as? SKSpriteNode
            
            if toque!.name ==  "texture_backGame" {
                
                let efeito = SKTransition.doorwayWithDuration(1.0)
                let voltaCena = GameScene(fileNamed: "GameScene")
                self.view?.presentScene(voltaCena!, transition: efeito)
                        
            }
        }
    }
}
